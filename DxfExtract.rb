# -*- coding: utf-8 -*-
=begin
=end

$KCODE = 'u'

require 'mscorlib'
require 'win32ole'
require 'System.Core'

#AcadRemoconオブジェクト作成
$acad = WIN32OLE.new 'AcadRemocon.Body'

#エラー処理
def er()
  #require 'Microsoft.VisualBasic,version="10.0.0.0",publicKeyToken="b03f5f7f11d50a3a",fileVersion="10.0.30319.1",culture="neutral"'
  load_assembly 'Microsoft.VisualBasic'
  include Microsoft::VisualBasic
  #ユーザーによるキャンセル
  #vbObjectError = 0xf80040000
  if ($acad.ErrNumber == Constants.vbObjectError + 1000) then
    #ここにキャンセル時の処理を追加
  else
    #エラー内容表示
    $acad.ShowError
  end
end

#選択した直線を50mm平行移動
def main()
  #図形選択→DXFファイル書き出し（ロックされた画層で描かれた図形は選択不可）
  unless $acad.acDxfOut "線分を選択", nil, false then er; exit end
  cnt = System::Runtime::CompilerServices::StrongBox[Fixnum].new
  extarr = System::Runtime::CompilerServices::StrongBox[Object].new
  #DXFファイルからLINEオブジェクトを抽出（10=始点X座標のグループコード,11=終点X座標のグループコード）
  unless $acad.DxfExtract cnt, extarr, "ENTITIES", "", "LINE", "10|11" then er; exit end
  #抽出数が0なら終了
  exit if cnt.value == 0
  #始点と終点のX座標を50mmずらす
  for i in 1 .. cnt.value
    extarr.value.set_value extarr.value.get_value(1, i).to_f + 50.0, 1, i
    extarr.value.set_value extarr.value.get_value(2, i).to_f + 50.0, 2, i
  end
  #配列への変更をDXFファイルに反映
  unless $acad.DxfUpdate extarr then er; exit end
  #DXFIN実行
  unless $acad.acDxfIn then er; exit end
  #直前の選択セットを削除
  unless $acad.acPostCommand "ERASE P^M^M" then er; exit end
end

#メイン
main
